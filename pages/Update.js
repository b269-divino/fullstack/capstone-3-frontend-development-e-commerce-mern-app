

import { useState,useContext,useEffect } from "react";
import { Form, Button, Card } from "react-bootstrap";
import {/*Navigate,*/ useNavigate, useParams} from 'react-router-dom';

import Swal from 'sweetalert2';


import UserContext from "../UserContext"
export default function UpdateProduct() 
{

  const [name, setName] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState("");

  // const [product, setProduct] = useState({});

  const {user} = useContext(UserContext);
  const navigate = useNavigate();
  const {productId} = useParams();

  function ProductUpdate(e)
  {
    e.preventDefault();

    //since the function takes the string value of the category and turns it into array, it's important to put conditionals for in case the admin does not update category. Without this, there will be an error due to trying to split an array instead of a string.
    
    fetch(`${process.env.REACT_APP_API_URL}/update/${productId}/update`,
    {
      method: "PUT",
      headers:
            {
              "Content-Type" : "application/json",
              Authorization: `Bearer ${localStorage.getItem("token")}`
            },
      body: JSON.stringify(
            {

              name: name,
              description: description,
              price: price,

            }) 
    }).then(res =>res.json()).then(data =>
    {
      // console.log(data)
      if (data.error)
      {
        Swal.fire({
          title: `${data.error}`,
                  icon: "error",
                  // text: "Please provide a different email."
        })
      }
      else
      {
        Swal.fire({
                  title: `${data.success}`,
                  icon: "success",
                  // text: "Please proceed to login."
                })
        
        navigate(`/products/${productId}`);
      }
    })
  
  };

  // Fetching current product details to set getters:
  

  return (
    <div className="d-flex justify-content-center mt-5">
      <Card style={{ width: "25rem" }}>
        <Card.Body>
          <Card.Title>Update Product</Card.Title>
          <Form onSubmit={(e) => UpdateProduct(e)}>

            

            <Form.Group controlId="formProductName">
              <Form.Label>Product Name</Form.Label>
              <Form.Control
                type="text"
                placeholder="Enter product name"
                value={name}
                onChange={(e) => setName(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group controlId="formProductDescription">
              <Form.Label>Product Description</Form.Label>
              <Form.Control
                as="textarea"
                rows={3}
                placeholder="Enter product description"
                value={description}
                onChange={(e) => setDescription(e.target.value)}
                required
              />
            </Form.Group>

            <Form.Group controlId="formProductPrice">
              <Form.Label>Price</Form.Label>
              <Form.Control
                type="number"
                placeholder="Enter price"
                value={price}
                onChange={(e) => setPrice(e.target.value)}
                required
              />
            </Form.Group>

            
            

            <Button variant="primary" type="submit">
              Update Product
            </Button>

          </Form>
        </Card.Body>
      </Card>
    </div>
  );
}

 // function UpdateProduct(e)
 //  {
 //    e.preventDefault(); {
 //    fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/update`,
 //    {
 //      method: "PATCH",
 //      headers: {
 //        "Content-Type": "application/json",
 //        Authorization: `Bearer ${localStorage.getItem("token")}`,
 //      },
 //      body: JSON.stringify({
 //        name: name,
 //        description: description,
 //        price: price,
 //      }),
 //    })
 //      .then((res) => {
 //        if (res.status === 200) {
 //          Swal.fire({
 //            title: "Product Enabled!",
 //            icon: "success",
 //            text: `${productName} is now enabled.`,
 //          });
 //          // fetchData();
 //        } else {
 //          Swal.fire({
 //            title: "Enable Product Failed!",
 //            icon: "error",
 //            text: `Something went wrong. Please try again later!`,
 //          });
 //        }
 //      });
 //  };

// useEffect(()=>
//   {
//     fetch(`${process.env.REACT_APP_API_URL}/products/product/${productId}`)
//           .then((res) => res.json())
//           .then((data) => {

//             // console.log(data);

//             setName(data.name);
//             sesetPrice(data.price);
//             setCategory(data.category);
//             setSalePercent(data.salePercent);
//             setStock(data.stock);

//             // setProduct(data);
//           });
//   },[productId])

 // This following section will display the form that takes the input from the user.
//  return (
//   <div className="mt-3 mb-3 justify-content-md-center">
//       <Card lg={{span: 6, offset:3}}>
//         <Card.Body>
//           <Card.Title>Update Product</Card.Title>
//           <Form onSubmit={(e) => updateProducts(e)}>


           

//             <Form.Group controlId="formProductName">
//               <Form.Label>New Product Name</Form.Label>
//               <Form.Control
//                 type="text"
//                 placeholder="Enter product name"
//                 value={name}
//                 onChange={(e) => setName(e.target.value)}
//                 required
//               />
//             </Form.Group>

//             <Form.Group controlId="formProductDescription">
//               <Form.Label>New Product Description</Form.Label>
//               <Form.Control
//                 as="textarea"
//                 rows={3}
//                 placeholder="Enter product description"
//                 value={description}
//                 onChange={(e) => setDescription(e.target.value)}
//                 required
//               />
//             </Form.Group>

//             <Form.Group controlId="formProductPrice">
//               <Form.Label>New Price</Form.Label>
//               <Form.Control
//                 type="number"
//                 placeholder="Enter price"
//                 value={price}
//                 onChange={(e) => setPrice(e.target.value)}
//                 required
//               />
//             </Form.Group>

          


//             <Button variant="primary" type="submit">
//               Update
//             </Button>

//           </Form>
//         </Card.Body>
//       </Card>
//     </div>
//   );
// }
