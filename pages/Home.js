import Banner from '../components/Banner';
import Highlights from '../components/Highlights';


export default function Home() {

	const data = {
		title: "Zuitt BikeShop",
		content: "Opportunities for everyone, everywhere.",
		destination: "/products",
		label: "See Products!"
	}


	return (
		<>
		<Banner data={data} />
    	<Highlights />
    	
		</>
	)
}


